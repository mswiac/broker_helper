name := """broker_helper"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.4"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test
)

libraryDependencies ++= Seq(
  "org.webjars" % "bootstrap" % "3.3.4",
  "com.github.scala-incubator.io" % "scala-io-file_2.11" % "0.4.3",
  "com.github.scala-incubator.io" % "scala-io-core_2.11" % "0.4.3",
  "org.jfree" % "jfreechart" % "1.0.19",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  "commons-io" % "commons-io" % "2.4"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
