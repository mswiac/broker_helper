package controllers

import java.io.File
import model.BrokerUtils
import play.api.mvc.BodyParsers.parse
import play.api.mvc._
import scalax.file.Path
import org.apache.commons.io._


class Application extends Controller {

  var pathToFile: Path = Path("")

  def index = Action {
    Ok(views.html.index())
  }

  def about = Action {
    Ok(views.html.about())
  }

  def contact = Action {
    Ok(views.html.contact())
  }

  def uploadFile = Action(parse.multipartFormData) { request =>
    request.body.file("fileUpload").map { uploadedFile =>
      val filename = uploadedFile.filename
      pathToFile = Path("uploadedFiles/" + filename)
      pathToFile.createFile(failIfExists = false)
      val file = new File(pathToFile.path)
      uploadedFile.ref.moveTo(file, replace = true)

      val brokerUtils = new BrokerUtils(FilenameUtils.removeExtension(file.getName))
      val stockData = brokerUtils.readInputFile(file)

      val chart = brokerUtils.getCandleStickChart(stockData, 90)
      val chartPath = brokerUtils.saveChartToFile(chart, "candles")
      Ok(views.html.upload(chartPath))
    }.getOrElse {
      Redirect(routes.Application.index)
    }
  }

  def movingAverage() = Action{
    val file = new File(pathToFile.path)
    val brokerUtils = new BrokerUtils(FilenameUtils.removeExtension(file.getName))
    val stockData = brokerUtils.readInputFile(file)
    val average = brokerUtils.movingAverage(stockData, 90)
    val extremas = brokerUtils.findExtremas(average)

    val candlesChart = brokerUtils.getCandleStickChart(stockData, 90)
    val candlesPath = brokerUtils.saveChartToFile(candlesChart, "candles")

    val series = brokerUtils.createSeries(average)
    val dataset = brokerUtils.createDataset(series)
    val chart = brokerUtils.createChart(dataset, "Average movement", extremas)
    val chartPath = brokerUtils.saveChartToFile(chart, "movAvg")

    Ok(views.html.movAvg(chartPath, candlesPath))
  }

  def bearsPower() = Action{
    val file = new File(pathToFile.path)
    val brokerUtils = new BrokerUtils(FilenameUtils.removeExtension(file.getName))
    val stockData = brokerUtils.readInputFile(file)

    val candlesChart = brokerUtils.getCandleStickChart(stockData, 90)
    val candlesPath = brokerUtils.saveChartToFile(candlesChart, "candles")

    val power = brokerUtils.bearsPower(stockData, 90)
    val extremas = brokerUtils.findExtremas(power)
    val series = brokerUtils.createSeries(power)
    val dataset = brokerUtils.createDataset(series)
    val chart = brokerUtils.createChart(dataset, "Bears power", extremas)
    val chartPath = brokerUtils.saveChartToFile(chart, "bearsPow")

    Ok(views.html.bears(chartPath, candlesPath))
  }

  def bullsPower() = Action{
    val file = new File(pathToFile.path)
    val brokerUtils = new BrokerUtils(FilenameUtils.removeExtension(file.getName))
    val stockData = brokerUtils.readInputFile(file)

    val candlesChart = brokerUtils.getCandleStickChart(stockData, 90)
    val candlesPath = brokerUtils.saveChartToFile(candlesChart, "candles")

    val power = brokerUtils.bullsPower(stockData, 90)
    val extremas = brokerUtils.findExtremas(power)
    val series = brokerUtils.createSeries(power)
    val dataset = brokerUtils.createDataset(series)
    val chart = brokerUtils.createChart(dataset, "Bulls power", extremas)
    val chartPath = brokerUtils.saveChartToFile(chart, "bullsPow")

    Ok(views.html.bulls(chartPath, candlesPath))
  }

  def data() = Action{
    val file = new File(pathToFile.path)
    val brokerUtils = new BrokerUtils(FilenameUtils.removeExtension(file.getName))
    val stockData = brokerUtils.readInputFile(file)
    val chart = brokerUtils.getCandleStickChart(stockData, 90)
    val chartPath = brokerUtils.saveChartToFile(chart, "candles")

    Ok(views.html.data(chartPath))
  }
}
