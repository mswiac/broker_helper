package model

class StockData(dateArray: Array[(Int, Int, Int)], timeArray: Array[(Int, Int)], closeArray: Array[Double], highArray: Array[Double], lowArray: Array[Double], openArray: Array[Double], volumeArray: Array[Double]) {

val date = dateArray
var time = timeArray
var close = closeArray
var high = highArray
var low = lowArray
var open = openArray
var volume = volumeArray

  def getDate = date
  def getTime = time
  def getClose = close
  def getHigh = high
  def getLow = low
  def getOpen = open
  def getVolume = volume
}
