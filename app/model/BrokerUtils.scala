package model

import java.awt.Color
import java.io._
import java.util.Date

import org.jfree.chart.axis.NumberAxis
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
import org.jfree.date.DateUtilities
import org.jfree.chart.{JFreeChart, ChartUtilities, ChartFactory}
import org.jfree.data.xy.{XYDataset, XYSeriesCollection, XYSeries, DefaultHighLowDataset}
import scalax.io.Resource
import scala.collection.mutable.ArrayBuffer

class BrokerUtils(name: String) {

  val filename = name

  def readInputFile(file: File): StockData = {
    implicit val codec = scalax.io.Codec.UTF8

    val resource = Resource.fromReader(new BufferedReader(new FileReader(file.getAbsolutePath)))
    val records = resource.lines().map(_ split ',').toArray

    var date = ArrayBuffer[(Int, Int, Int)]()
    var time = ArrayBuffer[(Int, Int)]()
    var close = ArrayBuffer[Double]()
    var high = ArrayBuffer[Double]()
    var low = ArrayBuffer[Double]()
    var open = ArrayBuffer[Double]()
    var volume = ArrayBuffer[Double]()

    records.foreach { i =>
      date += getDateFromRecord(i.apply(0))
      time += getTimeFromRecord(i.apply(1))
      close += i.apply(2).toDouble
      high += i.apply(3).toDouble
      low += i.apply(4).toDouble
      open += i.apply(5).toDouble
      volume += i.apply(6).toDouble
    }

    val stockData = new StockData(date.toArray, time.toArray, close.toArray, high.toArray, low.toArray, open.toArray, volume.toArray)
    return stockData
  }

  def getCandleStickChart(stockData: StockData, days: Int): JFreeChart = {

    val close = stockData.getClose.takeRight(days)
    val high = stockData.getHigh.takeRight(days)
    val low = stockData.getLow.takeRight(days)
    val open = stockData.getOpen.takeRight(days)
    val volume = stockData.getVolume.takeRight(days)
    var date = ArrayBuffer[Date]()

    val minimumValue = low.min
    val maximumValue = high.max

    val stockDataSize = stockData.getDate.size - 1

    for (i <- (stockDataSize - days + 1) to stockDataSize) {
      date += DateUtilities.createDate(stockData.getDate.apply(i)._1, stockData.getDate.apply(i)._2, stockData.getDate.apply(i)._3, stockData.getTime.apply(i)._1, stockData.getTime.apply(i)._2)
    }

    val title = "Wykres z ostatnich " + days + " dni."

    val chart = plotCandleStickChart(new DefaultHighLowDataset("Daily", date.toArray, high, low, open, close, volume), title, minimumValue, maximumValue)
    return chart
  }

  def getDateFromRecord(record: String): (Int, Int, Int) = {
    val separateDate = record.split('.')
    val date = (separateDate(0).toInt, separateDate(1).toInt, separateDate(2).toInt)
    return date
  }

  def getTimeFromRecord(date: String): (Int, Int) = {
    val separateTime = date.split(":")
    return (separateTime(0).toInt, separateTime(1).toInt)
  }

  def plotCandleStickChart(dataset: DefaultHighLowDataset, title: String, minimumValue: Double, maximumValue: Double): JFreeChart = {
    val chart = ChartFactory.createCandlestickChart(title, "okres", "wartość", dataset, true)
    chart.getXYPlot.getRangeAxis.setRange(minimumValue * 0.97, maximumValue * 1.03)
    return chart
  }

  def saveChartToFile(chart: JFreeChart, name: String): String = {
    val chartPath = "public/images/" + filename + "_" + name + ".png"
    ChartUtilities.saveChartAsPNG(new File(chartPath), chart, 900, 450)
    return "images/" + filename + "_" + name + ".png"
  }

  def movingAverage(stockData: StockData, days: Int): Array[Double] = {
    val average = new Array[Double](days)
    for (i <- 0 to days - 1) {
      var sum = 0.0
      var p = 0
      for (j <- i to i + 14) {
        if (j < days) {
          sum = sum + stockData.getLow.takeRight(days)(j)
          p = p + 1
        }
      }
      sum = sum / p
      average(i) = sum
    }
    return average
  }

  def bearsPower(stockData: StockData, days: Int): Array[Double] = {
    val bearsPower = new Array[Double](days)
    for (i <- 0 to days - 1) {
      var sum = 0.0
      var p = 0
      for (j <- i to i + 14) {
        if (j < days) {
          sum = sum + stockData.getClose.takeRight(days)(j)
          p = p + 1
        }
      }
      sum = stockData.getLow.takeRight(days)(i) - sum / p
      bearsPower(i) = sum
    }
    return bearsPower
  }

  def bullsPower(stockData: StockData, days: Int): Array[Double] = {
    val bullPower = new Array[Double](days)
    for (i <- 0 to days - 1) {
      var sum = 0.0
      var p = 0
      for (j <- i to i + 14) {
        if (j < days) {
          sum = sum + stockData.getClose.takeRight(days)(j)
          p = p + 1
        }
      }
      sum = stockData.getHigh.takeRight(days)(i) - sum / p
      bullPower(i) = sum
    }
    return bullPower
  }

  def createSeries(data: Array[Double]): XYSeries = {
    val series = new XYSeries("First")
    for (i <- 1 to data.length) {
      series.add(i, data(i - 1))
    }
    return series
  }

  def createDataset(data: Array[XYSeries]): XYSeriesCollection = {
    val dataset = new XYSeriesCollection()
    data.foreach { i =>
      dataset.addSeries(i)
    }
    return dataset
  }

  def createDataset(data: XYSeries): XYSeriesCollection = {
    val dataset = new XYSeriesCollection()
    dataset.addSeries(data)
    return dataset
  }

  def createChart(dataset: XYDataset, title: String, extremas: (Double, Double)): JFreeChart = {
    val chart = ChartFactory.createXYLineChart(title, "X", "Y", dataset, PlotOrientation.VERTICAL, true, true, false)
    chart.setBackgroundPaint(Color.white)
    val plot = chart.getXYPlot()
    val domain = plot.getRangeAxis()
    domain.setRange(extremas._1 * 0.97, extremas._2 * 1.03)
    plot.setBackgroundPaint(Color.lightGray)
    plot.setDomainGridlinePaint(Color.white)
    plot.setRangeGridlinePaint(Color.white)
    val renderer = new XYLineAndShapeRenderer()
    renderer.setSeriesLinesVisible(0, false)
    renderer.setSeriesShapesVisible(1, false)
    plot.setRenderer(renderer)

    return chart
  }

  def findExtremas(array: Array[Double]): (Double, Double) = {
    val minimumValue = array.min
    val maximumValue = array.max
    return (minimumValue, maximumValue)
  }
}
